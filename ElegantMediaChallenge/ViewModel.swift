//
//  ViewModel.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-17.
//

import Foundation

class ViewModel {
    
    // Create an array to store the all the data
    var retrievedData: [RetrievingData] = []
    
    // Create a function to fetch data
    func fetchData(completion: @escaping () -> Void) {
        let url = "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/hotels.json"
        
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            guard let data = data, error == nil else {
                print("Failed to fetch data with error \(error!.localizedDescription)")
                return
            }
            
            // Decode the json
            var result: Response?
            do {
                result = try JSONDecoder().decode(Response.self, from: data)
            } catch {
                print("failed to decode data with error \(error.localizedDescription)")
            }
            
            guard let json = result else {
                return
                
            }
            // Loop over the array and add the items to the data array
            for item in json.data {
                let dataItem = RetrievingData(title: item.title, description: item.description, imageUrl: item.image.small, address: item.address, latitude: Double(item.latitude)!, longitude: Double(item.longitude)!)
                self.retrievedData.append(dataItem)
            }
            completion()
        }.resume()
    }
}


// MARK: - Response
struct Response: Codable {
    let status: Int
    let data: [Result]
}

// MARK: - Result
struct Result: Codable {
    let id: Int
    let title, description, address, postcode: String
    let phoneNumber, latitude, longitude: String
    let image: Image

    enum CodingKeys: String, CodingKey {
        case id, title
        case description
        case address, postcode, phoneNumber, latitude, longitude, image
    }
}

// MARK: - Image
struct Image: Codable {
    let small, medium, large: String
}
