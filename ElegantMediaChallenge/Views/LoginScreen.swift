//
//  LoginScreen.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-17.
//

import FBSDKLoginKit

class LoginScreen: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        
        // Check if user already logged in
        if let token = AccessToken.current, !token.isExpired { // User is logged in
            logUserIn(token: token.tokenString)
            
        } else {
            setupLoginButton()
        }
    }
    
    // Create a function to setup the login button
    func setupLoginButton() {
        let loginButton = FBLoginButton()
        loginButton.delegate = self
        loginButton.center = view.center
        
        loginButton.permissions = ["public_profile", "email"]
        view.addSubview(loginButton)
    }
    
    // Preset homeScreen
    func presentHomeScreen(name: String, email: String) {
        let homeScreen = HomeScreen()
        
        homeScreen.userName = name
        homeScreen.userEmail = email
        
        let screenToBePresented = UINavigationController(rootViewController: homeScreen)
        
        screenToBePresented.modalPresentationStyle = .fullScreen
        present(screenToBePresented, animated: true)
    }
    
    // Create a function to log user in
    func logUserIn(token: String?) {
        
        let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "email, name"], tokenString: token, version: nil, httpMethod: .get)
        
        request.start(completion: { connection, result, error in
            if let userDict = result as? [String : String] {
                
                // Go to HomeScreen
                self.presentHomeScreen(name: userDict["name"]!, email: userDict["email"]!)
            }
        })
    }
}

extension LoginScreen: LoginButtonDelegate {
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        
        let token = result?.token?.tokenString
        logUserIn(token: token)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) { }
}

