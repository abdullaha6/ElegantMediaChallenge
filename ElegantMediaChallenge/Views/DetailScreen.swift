//
//  DetailScreen.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-18.
//

import UIKit

class DetailScreen: UIViewController {

    // Create a variable to store the selected object
    var dataPassed: RetrievingData = RetrievingData(title: "", description: "", imageUrl: "", address: "", latitude: 0, longitude: 0)
    
    // Create the navigation bar button
    let mapButton: UIButton = {
        
        let button = UIButton()
        button.setImage(UIImage(named: "mapIcon"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    // Create the image view
    let catImageView: UIImageView = {

        let iv = UIImageView(image: UIImage(named: "dog"))
        iv.backgroundColor = .green
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 15
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    // Create the title text label
    let titleLabel: UILabel = {

        let label = UILabel()
        label.text = "Title"
        label.textColor = UIColor(red: 205/255, green: 97/255, blue: 85/255, alpha: 1)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Heavy", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the description text label
    let descriptionLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Address"
        label.textAlignment = .justified
        label.numberOfLines = 0
        label.textColor = .gray
        label.font = UIFont(name: "Avenir", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScreen()
        setupBarButtonItems()
        addCatImageViewConstraints()
        addTitleLabelConstraints()
        addDescriptionLabelConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Assign new data
        titleLabel.text = dataPassed.title
        descriptionLabel.text = dataPassed.description
    }
    
    // MARK: Setups
    // Setup screen
    func setupScreen() {
        title = "Details"
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground

        } else {
            view.backgroundColor = .white
        }
    }
    
    func setupBarButtonItems() {
        
        mapButton.addTarget(self, action: #selector(mapButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: mapButton)
        // Set the width
        mapButton.frame = CGRect(x: 0, y: 0, width: 20, height: 25.7)
        mapButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    // MARK: Constraints
    // Add the constraints to the image view
    func addCatImageViewConstraints() {
        
        view.addSubview(catImageView)
        if #available(iOS 11.0, *) {
            catImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        } else {
            catImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        }
        catImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        catImageView.widthAnchor.constraint(equalToConstant: 300).isActive = true
        catImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    // Add the constraints to the title label
    func addTitleLabelConstraints() {
        
        view.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: catImageView.bottomAnchor, constant: 25).isActive = true
        if #available(iOS 11.0, *) {
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        } else {
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        }
    }
    
    // Add the constraints to the description label
    func addDescriptionLabelConstraints() {
        
        view.addSubview(descriptionLabel)
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15).isActive = true
        if #available(iOS 11.0, *) {
            descriptionLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        } else {
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        }
    }
    
    // MARK: Selectors
    // When the map button is tapped
    @objc func mapButtonTapped() {
        // Push the map view controller
        let mapScreen = Map()
        
        mapScreen.dataPassed = dataPassed
        self.navigationController?.pushViewController(mapScreen, animated: true)
    }
}
