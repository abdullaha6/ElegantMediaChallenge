//
//  Map.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-18.
//

import UIKit
import MapKit

class Map: UIViewController {
    
    // Create a variable to store the selected object
    var dataPassed: RetrievingData = RetrievingData(title: "", description: "", imageUrl: "", address: "", latitude: 0, longitude: 0)
    
    let mapView: MKMapView = {
        let mv = MKMapView()
        mv.translatesAutoresizingMaskIntoConstraints = false
        return mv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScreen()
        addMapViewConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showLocation()
    }
    
    // MARK: Setups
    // Setup screen
    func setupScreen() {
        
        title = "Map"
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground

        } else {
            view.backgroundColor = .white
        }
//        navigationController?.navigationBar.tintColor = UIColor(red: 205/255, green: 97/255, blue: 85/255, alpha: 1)
    }
    
    // MARK: Constraints
    // Add the constraints to the map view
    func addMapViewConstraints() {
        
        view.addSubview(mapView)
        if #available(iOS 11.0, *) {
            mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            mapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        } else {
            mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        }
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    // Create a function to show the location on map
    func showLocation() {
        
        let lat = CLLocationDegrees(dataPassed.latitude)
        let long = CLLocationDegrees(dataPassed.longitude)
        
        let annotation = MKPointAnnotation()
        annotation.title = dataPassed.title
        annotation.subtitle = dataPassed.address
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        mapView.addAnnotation(annotation)
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
}
