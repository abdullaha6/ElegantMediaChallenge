//
//  CustomTableViewCell.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-17.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    var tableViewCellData: RetrievingData? {
        didSet {
            guard let cellData = tableViewCellData else { return }
            
            // Update the cell elements
//            downloadImage(from: cellData.imageUrl)
            titleLabel.text = cellData.title
            addressLabel.text = cellData.address
        }
    }

    static let identifier = "CusomTableViewCell"
    
    // Create the image view
    let catImageView: UIImageView = {

        let iv = UIImageView(image: UIImage(named: "dog"))
        iv.backgroundColor = .green
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 37.5
        iv.clipsToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    // Create the title text label
    let titleLabel: UILabel = {

        let label = UILabel()
        label.text = "Title"
        label.textAlignment = .left
//        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Heavy", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the address text label
    let addressLabel: UILabel = {
        
        let label = UILabel()
        label.text = "Address"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = .gray
        label.font = UIFont(name: "Avenir", size: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Set elements to nil to avoid duplication
    override func prepareForReuse() {
        super.prepareForReuse()
        
//        catImageView.image = nil
        titleLabel.text = nil
        addressLabel.text = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add the elements to the cell
        contentView.addSubview(catImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(addressLabel)

        // Add the constraints
        catImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        catImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        catImageView.widthAnchor.constraint(equalToConstant: 75).isActive = true
        catImageView.heightAnchor.constraint(equalToConstant: 75).isActive = true

        titleLabel.leadingAnchor.constraint(equalTo: catImageView.trailingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -1).isActive = true
        
        addressLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 1).isActive = true
        addressLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
    }
    
    // Create a function to download the images
    func downloadImage(from urlString: String) {
        let url = URL(string: urlString)!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                print("Failed to fetch image data with error \(error!.localizedDescription)")
                return
            }
            print(data)
            // Update UI from main thread
            DispatchQueue.main.async() { [weak self] in
                self?.catImageView.image = UIImage(data: data)
            }
        }.resume()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
