//
//  HomeScreen.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-17.
//

import UIKit
import FBSDKLoginKit

class HomeScreen: UIViewController {
    
    // Create variables to store the data that's passed
    var userName: String = ""
    var userEmail: String = ""
    
    // Create the name label
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Abdullah Ajmal"
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the email label
    let emailLabel: UILabel = {
        let label = UILabel()
        label.text = "abdullaha6@gmail.com"
        label.font = UIFont(name: "Avenir", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Create the logout button
    let logoutButton = FBLoginButton()
    
    // Create the table view
    let tableView: UITableView = {
        let tv = UITableView()
        tv.register(CustomTableViewCell.self, forCellReuseIdentifier: CustomTableViewCell.identifier)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    // Create an instance of the view model
    let viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScreen()
        addNameLabelConstraints()
        addEmailLabelConstraints()
        setupLogoutButton()
        setupTableView()
        
        viewModel.fetchData {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = userName
        emailLabel.text = userEmail
    }
    
    // MARK: Setups
    // Setup screen
    func setupScreen() {
        title = "List View"
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground

        } else {
            view.backgroundColor = .white
        }
        
        // Navigation bar customization
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.backgroundColor = UIColor(red: 205/255, green: 97/255, blue: 85/255, alpha: 1)
        UIApplication.shared.statusBarUIView?.backgroundColor = UIColor(red: 205/255, green: 97/255, blue: 85/255, alpha: 1)
        
    }
    
    // Setup the logout button
    func setupLogoutButton() {
        
        logoutButton.delegate = self
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logoutButton)
        
        addLogoutButtonConstraints()
    }
    
    // Setup the table view
    func setupTableView() {
        
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        
        addTableViewConstraints()
    }
    
    // MARK: Constraints
    // Add constraints to the name label
    func addNameLabelConstraints() {
        
        view.addSubview(nameLabel)
        if #available(iOS 11.0, *) {
            nameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        } else {
            nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        }
        nameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add constraints to the email label
    func addEmailLabelConstraints() {
        
        view.addSubview(emailLabel)
        emailLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 7).isActive = true
        emailLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add the constraints to the logout button
    func addLogoutButtonConstraints() {
        
        logoutButton.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 15).isActive = true
        logoutButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // Add the constraints to the table view
    func addTableViewConstraints() {
        
        tableView.topAnchor.constraint(equalTo: logoutButton.bottomAnchor, constant: 20).isActive = true
        if #available(iOS 11.0, *) {
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
    }
}
