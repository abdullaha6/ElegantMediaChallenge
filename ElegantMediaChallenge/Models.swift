//
//  Models.swift
//  ElegantMediaChallenge
//
//  Created by Mohammed Ajmal on 2022-08-17.
//

import Foundation

struct RetrievingData {
    
    let title: String
    let description: String
    let imageUrl: String
    let address: String
    let latitude: Double
    let longitude: Double
}
