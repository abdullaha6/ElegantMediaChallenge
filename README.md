# Elegant Media Challenge

### Running the application
To run the application, clone the repo and open the `ElegantMediaChallenge.xcworkspace` on XCode and run it. To get the best out of the application, run it using an **iPhone 8** simulator.
